---
date: 2019-09-03
title: Git - Tutorial 3
categories:
  - Git
description: "How to create and use branches and tags"
type: Document
set: hae806e
set_order: 13
highlighter: rouge
---

## Objective

In this third tutorial will teach you how to create and manipulate branches as well as creating tags.

If you need a quick refresher on what a branch is and what it is useful, you can check the [Git basics page](/git/hae806e-git/#branches).

We will use the *git-tutorial* project we made in the first tutorial as a basis for this one.

## Create a branch

Let's create a feature branch called *todo* which will be used to incorporate a TODO list in the project:
```bash
git branch todo
```

This has the effect of creating a new branch called *todo* but you are still on the *master* branch.
To switch to this new branch, you can use the `checkout` command:
```bash
git checkout todo
```

If you run the `status` command, you will see that you are on the *todo* branch now:
```
On branch todo
nothing to commit, working tree clean
```

## Modify the branch

Add a new file called *TODO.md* and put a list of things you want to do inside.
Then commit these changes.

If you run the `log` command you should see something like this:
```
commit 701fa5ac0bcb1851c5e6d7a770db068d26801b4f (HEAD -> todo)
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 15:00:08 2019 +0200

    List of things to do

commit 870a336338b379f2ba5ca2cb2c5b55909a9efdda (origin/master, master)
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 07:40:42 2019 +0000

    Identity update

commit bb85864f9ca18a06a681f4cb6e4ed1db58a95274
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Mon Sep 2 18:26:42 2019 +0200

    Adding a readme file to the project

    This is a Markdown file. If you need a refresher about Markdown, please visit https://www.markdownguide.org
```

We can see that the tip of the *master* branch is still at the previous commit whereas the *todo* branch points to the latest commit.
This confirms us that *master* is behind *todo* since it doesn't include our last commit.

Switching back to the *master* branch will confirm this:
```bash
git checkout master
ls
> README.md
```

The *TODO.md* file is not here anymore.
But don't worry, it still exists inside the Git history so it is not lost!

## Merge the feature branch

We could go on a create more commits on the *todo* branch (and you can if you want) but let's say our feature is completed and ready to be part of the *master* branch.

While on the master *branch*, just use the `merge` command to include the content of the *todo* branch:
```bash
git merge todo
```

Which should provide an output similar to this:
```
Updating 870a336..701fa5a
Fast-forward
 TODO.md | 4 ++++
 1 file changed, 4 insertions(+)
 create mode 100644 TODO.md
```
that says the file *TODO.md* has been created using a fast-forward merge.
Here a fast-forward merge occurred because the *master* branch didn't change since the branching occurred.
In this case, Git can simply adds the new commits at the end of the master branch.

However, if the *master* branch has changed since the initial branching, then conflicts can occur and a new commit must be created to reconcile the changes.

Let's see how it goes in these not ideal cases.

## Solve a conflict

Here we will make two modifications, one in the *master* branch and one in the *todo* branch.
We will modify the same part of the same file in order to create a conflict during the merge.
This is not something you want to do willingly in normal times but this situation can happen if someone commits directly to the *master* branch or if another feature branch is merged before yours.

While we are on the *master* branch, modify the first line or the *TODO.md* file and commit the change.
Now, switch to the *todo* branch, make a different change on that same line and commit the change.
Switch back to *master* and merge the *todo* branch:
```bash
git merge todo
```

Which should provide an output similar to this:
```
Auto-merging TODO.md
CONFLICT (content): Merge conflict in TODO.md
Automatic merge failed; fix conflicts and then commit the result.
```

As expected, a conflict occurred because both branches contained modifications to the same line of the same file.
Git can't solve this issue alone, you have to solve it yourself.
Let's first check what is now the content of *TODO.md*:
```
<<<<<<< HEAD
Here is what need to be done URGENTLY:
=======
Here is what need to be done very quickly:
>>>>>>> todo
 * Understand what Git is
 * Know how to use Git
 * Use Git all the time
```

You can see that the conflicting part is enclosed with `<<<<<<<` and `>>>>>>>` and that the two versions are separated using `=======`.
The top part is what was on the *master* branch before the merge, and the second is what comes from the *todo* branch.
You have to modify the file to solve the conflict by removing the conflict delimiters and choosing if you keep one version or the other, or a mix between the two.
Here we can go with the incoming change and thus leave only the second part:
```
Here is what need to be done very quickly:
 * Understand what Git is
 * Know how to use Git
 * Use Git all the time
```

Now that the conflict has been resolved, commit the change in order to close the merging process.

Before moving to the next part, let's take a look at the commit history using `git log --graph`:
```
*   commit 4f3976430d01a7f4025077bd037e96d6943e8643 (HEAD -> master)
|\  Merge: 86b6471 19cffaf
| | Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
| | Date:   Tue Sep 3 16:16:57 2019 +0200
| |
| |     Discarding direct modifications of the master branch
| |
| * commit 19cffaf29911d6c828dd81f78294ae0b9f5b25f7 (todo)
| | Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
| | Date:   Tue Sep 3 16:06:11 2019 +0200
| |
| |     Making the todo list more urgent
| |
* | commit 86b64717f2a64fba610e1b0bf7379d42c684b242
|/  Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
|   Date:   Tue Sep 3 16:04:22 2019 +0200
|
|       Increasing priority
|
* commit 701fa5ac0bcb1851c5e6d7a770db068d26801b4f
| Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
| Date:   Tue Sep 3 15:00:08 2019 +0200
|
|     List of things to do
|
* commit 870a336338b379f2ba5ca2cb2c5b55909a9efdda (origin/master)
| Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
| Date:   Tue Sep 3 07:40:42 2019 +0000
|
|     Identity update
|
* commit bb85864f9ca18a06a681f4cb6e4ed1db58a95274
  Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
  Date:   Mon Sep 2 18:26:42 2019 +0200

      Adding a readme file to the project

      This is a Markdown file. If you need a refresher about Markdown, please visit https://www.markdownguide.org
```

Here we can visualize the branching process since the *Making the todo list more urgent* commit is on a different branch, created before the *Increasing priority* commit and that was later merged into *master* by the commit *Discarding direct modifications of the master branch*.

## Create tags

The sole use of tags is to associate a meaningful and more general name to a commit.
This is commonly used to identify versions of the project.

Let's say that our project reached version 1.0 and we want to tag it as such.
To do so, simply use the `tag` command:
```bash
git tag v1.0
```

You can check the current list of tags by running the `tag` command without any argument.
Tags are also indicated in the output of the `log` command:
```
commit 4f3976430d01a7f4025077bd037e96d6943e8643 (HEAD -> master, tag: v1.0)
Merge: 86b6471 19cffaf
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 16:16:57 2019 +0200

    Discarding direct modifications of the master branch

commit 19cffaf29911d6c828dd81f78294ae0b9f5b25f7 (todo)
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 16:06:11 2019 +0200

    Making the todo list more urgent

[...]
```

Now, for the sake of the example, let's create a new commit follow by a new version tag:
```bash
echo "build/" > .gitignore
git add .gitignore
git commit -m "Adding a gitignore file" -m "Excluding the build folder"
git tag v1.1
```

You probably remember that we used the `checkout` command to switch branches earlier.
You will be pleased to learn that the same command can also be used with tags (and commit hashes for that matter).
So let's say that there is an issue with the version 1.1 and you want to go back to the 1.0.
It's now as easy as:
```bash
git checkout v1.0
```

Without tags, it would require to read through the commit history, find the good commit and then use the `checkout` command using its hash value.

To finish with this, push everything to your remote repository:
```bash
git push origin master
```

But if you go to your repository home page, you will notice that there is only one branch and no tags.
Strange isn't it?
Well, not really.
The default `push` command only pushes the commits for the given branch.
The rest is still local and it is up to you to decide if you want to push it as well.
To push the *todo* branch and the tags, you can use the following commands:
```bash
git push origin todo
git push origin --tags
```

Now your remote repository is fully synchronized with your local one.

## What we learned

In this third tutorial, we saw how to:
 1. Create a branch
 2. Modify its content
 3. Merging a branch
 4. Solve merge conflicts
 5. Create tags

This Git tutorial series is now over.
You are encouraged to used Git as much as possible, even if you are not explicitly asked to do so.
This will make you practice and is in general a good thing to do.