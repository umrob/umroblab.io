---
date: 2019-09-03
title: Git - Tutorial 2
categories:
  - Git
description: "How to fork a project and create merge requests"
type: Document
set: hae806e
set_order: 12
highlighter: rouge
---

## Objective

In this second tutorial will make you learn how to start from an already existing repository and to provide some modifications to it.
This is more related to collaborative working.

## Fork a repository

Here you will fork an already existing project.
As a reminder, a fork is a copy of a repository, meaning that you can work on your fork without impacting the original repository.
But you can still synchronize both repositories together either using multiple remotes or through merge requests, more on that later.

On the [2022-2023-students](https://gitlab.com/umrob/hae806e/2022-2023-students) project's home page, click the *Fork* button.
Here, you can select in which namespace you want to fork it.
Pick your personal one (the one named after you).
After a few seconds you should land on your fork's home page.
It is very similar to the original project's page except that under the description there is the *Forked from umrob/hae806e/2022-2023-students* mention.

That just what is needed to fork a project!

Now let's make some modifications.

## Clone a repository

In order to make modification to your forked project, you need to download it locally.
To to this, you can use the `clone` command and passing it the address of the repository:
```bash
git clone git@gitlab.com:namespace/2022-2023-students.git # Use your fork address
```

Once the operation is completed, you can enter the *2022-2023-students* folder and start modifying its content.

## Add your personal information

The original *2022-2023-students* repository will serve has a database containing which students are following this course.

To register yourself, please follow the instructions provided in the *README.md* file.
Once you're done, commit and push your changes.

## Create a merge request

Now you have to submit your changes to the original repository.
But there is a catch, you are not allowed to push to this repository.
Indeed, making a repository public will only allow people to consult its content, not modifying it.

There are two solutions for this issue:
  1. The owner of the repository gives you a write access to it
  2. You create a merge request

The first solution imply a complete trust to the person the owner is giving access too.
There are different permission levels to limit what a user can do, but still, people can make modifications without the owner being aware of.

In our case, this is not acceptable, we cannot allow one student to modify the information about another one.
To solve this issue, each student will create a merge request.
A merge request is a functionality offered by all common Git hosting platforms such as Gitlab.com but is not native to Git itself[^MR].
As its name suggests, it is a request sent to the owner of the original repository to merge your modifications into it.
That way, any user who forked the original repository can create merge requests but it is up to the original repository owner to accept or decline these requests.
This is how open source projects work.

To create a merge request, simply go to your fork's page, select *Merge Requests* in the left menu and hit *New merge request*.
Then, select which branch you want to use for the merge request (here it's *master*) and click on *Compare branches and continue*.
Now you have to give some details about your merge request:
 * A title. By default it is set to the latest commit
 * A description. Use it to give details about the purpose of this merge request
The rest can be left as is.
Once you're done, press the *Submit merge request* button.

At this point, the merge request is added to the original repository and the owner can inspect it.
If the owner doesn't agree or understand your modifications, it can start a conversation on the merge request's page.
If you add new commits to satisfy the owner's requirements, they will automatically added to the merge request.
Once everything is ready, the owner can accept the merge request and your changes are integrated in the original repository.

[^MR]: The equivalent of a merge request can of course be done manually using Git commands: adding the fork as a remote, fetching its content, inspecting the modifications, merging them if they are ok and then pushing everything back to the original repository. Using merge request is much easier and less error prone.

## Synchronize the fork

Now that your merge request has been accepted, you need to synchronize with the original repository before making any new modification.

To do so, let's create a new remote called *official* which points to the original repository so that we can pull from it:
```bash
git remote add official git@gitlab.com:umrob/hae806e/2022-2023-students.git
```

With that being done, we can pull from this remote:
```bash
git pull official master
```

And inspect the commit history, which should look like this:
```bash
commit 21885f88d8ac688fcf91d4b6256c7de502b2712e (HEAD -> master, official/master)
Merge: bbbf77c 7ec931c
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 11:15:18 2019 +0000

    Merge branch 'master' into 'master'

    Benjamin Navarro info

    See merge request umrob/hae806e/2022-2023-students!3

commit 7ec931c856b31811d811d096f075070d3491f5c3 (origin/master, origin/HEAD)
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 11:21:18 2019 +0200

    Benjamin Navarro info

commit bbbf77c17bb6e4d79ee3f7220244f2699e5fcccd
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 11:15:57 2019 +0200

    Template file for student info

commit 8bb5ceb6689df4d1b44dfb12968e04baac21864b
Author: Benjamin Navarro <navarro.benjamin13@gmail.com>
Date:   Tue Sep 3 11:15:35 2019 +0200

    Adding a readme file to the project

    Contains instructions for the students to follow
```

We can see that a special commit has been created during the merge process, giving details about the underlying merge request.

As other students' modifications are integrated into the repository, you can pull your *official* remote to get your local content up-to-date.
You can also push these modifications to your *official* remote to keep it up-to-date with the original repository.

And that's it, you now have the basic knowledge to collaborate to projects you doesn't own.

## What we learned

In this second tutorial, we saw how to:
 1. Fork a repository
 2. Clone a repository
 3. Update your fork
 4. Submit your changes to the original repository using a merge request
 5. Keeping your fork up-to-date with the original repository

In the next [tutorial](/git/hae806e-git-tutorial-3), we will see how branching and tagging work.
