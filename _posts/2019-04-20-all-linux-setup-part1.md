---
date: 2019-04-20
title: Linux environment setup
categories:
  - linux
description: "How to configure your Linux environment for the robotics classes"
type: Document
set: getting-started
set_order: 1
highlighter: rouge
---

## Operating system

We will use Linux as our primary operating system because it is free and widely used in robotics, for both classical and embedded computers.

As for the [distribution](https://en.wikipedia.org/wiki/Linux_distribution), we will focus on [Ubuntu](https://www.ubuntu.com/) (or one of its derivatives) because it is the only one where [ROS](http://www.ros.org/) is officially supported.

### Classic install (single/dual-boot)

If you're installing Linux directly on a descent computer, you can go with Ubuntu.
But if you plan to use an older/low-end computer or a [virtual machine](https://en.wikipedia.org/wiki/Virtual_machine), it is better to choose a more lightweight derivative such as [Xubuntu](https://xubuntu.org/)or [Lubuntu](https://lubuntu.net/).

The provided ROS2 packages have been tested with Ubuntu 20.04 (ROS2 Foxy) and Ubuntu 22.04 (ROS2 Humble), so you can pick one or the other.

From here, you have two possibilities.
Either you go though this guide and the linked tutorials to setup your system yourself (great learning experience) or you can go with a pre-made system with everything required in this course already installed.

You can download the premade system [here](https://seafile.lirmm.fr/f/780a6ef9703d4e8bb00f/?dl=1).

If you wan to install Ubuntu from scratch, instructions can be found [here](https://tutorials.ubuntu.com/tutorial/tutorial-install-ubuntu-desktop#0), or [here](https://www.tecmint.com/install-ubuntu-alongside-with-windows/) if a dual boot with Windows is required (you will need at least 20GB of free space to install Ubuntu and the software we need).

[^vm-requirements]: VirtualBox will run this Linux system alongside the operating system you are using so you need a computer powerful enough to support that. Plan for at least 2 cores and 2GB of RAM allocated to the virtual machine. This means that your computer should at least have 4 cores and 4GB of RAM to use the virtual machine. Graphical performance can be bad in VMs so use this if you have no other choice.

### WSL2 on Windows

If you have issues installing Ubuntu on your hard drive to boot it directly and you already have Windows 10 or 11 installed then you can try using WSL2.

WSL2 is some kind of lightweight virtual machine well integrated with Windows that allows to run various Linux distributions.

You can follow the official instruction [here](https://learn.microsoft.com/en-us/windows/wsl/install) to set it up.
Just make sure that you use the version 2.

Once it's done and you have a Linux terminal you can go on and follow the same instructions as the classical installation to set up your system.

Be aware that this is not tested by us and that we won't provide any support if you encounter problems with this method.

## C/C++ Tools

We first have to install all the tools required for C and C++ development. This includes:
 * Compilers: [GCC/G++](https://www.gnu.org/software/gcc/)
 * Debugger: [GDB](https://www.gnu.org/software/gdb/)
 * Build tool: [CMake](https://cmake.org/)
 * Versioning system: [Git](https://git-scm.com/)
 * Documentation generation: [Doxygen](http://www.doxygen.nl/)

Let's start by updating the list of available packages and upgrade the ones installed to the latest version. Open your terminal and enter:

~~~bash
sudo apt update
sudo apt upgrade
~~~

Now that the system is up to date, we can install everything:

~~~ bash
sudo apt install build-essential gdb cmake cmake-curses-gui git git-lfs doxygen
~~~

## ROS

The installation of ROS is described in this official [tutorial](https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html).
Please read it carefully and choose the *Desktop Install*.

## Code editor

There are plenty of code editors available, most of them being free.
It is up to you to choose one that you like but, to help you with that, here are a few must-have features:
 * C and C++ support: our main programming languages
 * CMake support: our main build system
 * Git: for version control
 * Cross-platform: you may have to use a different OS at some point so it is better if your editor works everywhere

If you prefer to have one editor for everything, [Visual Studio Code](https://code.visualstudio.com/) is a very good candidates. [Vim](https://www.vim.org/)/[Neovim](https://neovim.io/) are great if you never want to leave your terminal, but they have a steeper learning curve. These editors are basically just advanced text editors by default and thus require you to install plugins to support new languages, auto-completions, etc. This process is very easy but can take some time to find everything you need.

On the other hand, if you prefer full-featured IDEs that pack everything needed for a specific language, [Qt Creator](https://www.qt.io/download), [Eclipse](https://www.eclipse.org/) or [Clode::Blocks](http://www.codeblocks.org/) can do a good job.

If you have no idea on which one to choose, you can consult [this page](https://www.tecmint.com/best-linux-ide-editors-source-code-editors/) to have an overview of the most commonly used editors for C/C++. But beware, not all of them fit the three points listed above.

## (Optional) A better terminal emulator

You may want to install [Terminator](https://launchpad.net/terminator) if you wan to enjoy full customization of your terminal and splitting windows in multiple sub-terminals. If you are interested, just run:

~~~ bash
sudo apt install terminator
~~~

And if you want to set it as your default terminal, search for **Preferred Applications** in the applications menu, open it, and change the terminal field to **Terminator** (can be in a **Utility** tab). If you use Xfce (like with **Xubuntu** or **Linux Lite**) and want the **ctrl-alt-t** shortcut to open **Terminator**, follow [these instructions](https://askubuntu.com/questions/499563/cant-set-altctrlt-to-launch-terminal-in-xfce4).

## (Optional) A better shell

Since you will spend quite some time in a terminal using the shell, it can be a good idea to install something better than the default one (**Bash**) and customize it to your liking.

We recommend to install [ZSH](https://sourceforge.net/projects/zsh/) + [Oh My ZSH](https://ohmyz.sh/) to get the best shell experience possible:

~~~ bash
sudo apt install zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
~~~

The second instruction will install **Oh My ZSH** and ask for your password in order to set **ZSH** to be your default terminal.

Now you can browse the [available themes](https://github.com/robbyrussell/oh-my-zsh/wiki/Themes) and pick the one you like ([Agnoster](https://github.com/agnoster/agnoster-zsh-theme) is a great starter). You will most likely have to install patched fonts to display the theme correctly so please read your theme installation instructions carefully.

### Important notice
if you switch to **ZSH**, your configuration script is now `~/.zshrc` instead of `~/.bashrc`.
So if your read a tutorial telling you to put something in your `~/.bashrc` file, do it in your `~/.zshrc` instead.
